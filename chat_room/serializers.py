from rest_framework import serializers

from chat_room.models import Room , Chat
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    """User serializtion"""

    class Meta:
        model = User
        fields = ("id", "username")

class RoomSerializers(serializers.ModelSerializer):
    """Serialization of chat rooms"""
    creater = UserSerializer()
    invited = UserSerializer(many=True)
    class Meta:
        model = Room
        fields = ("id","creater", "invited", "date")

class ChatSerializers(serializers.ModelSerializer):
    """Chat serialization"""
    user = UserSerializer()
    class Meta:
        model = Chat
        fields = ("user","text","date")

class ChatPostSerializers(serializers.ModelSerializer):
    """Chat serialization"""
    class Meta:
        model = Chat
        fields = ("room","text")

