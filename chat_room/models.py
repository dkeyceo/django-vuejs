from django.db import models

from django.contrib.auth.models import User

class Room(models.Model):
    """Model of chat room"""

    creater = models.ForeignKey(User, verbose_name="Creater", on_delete=models.CASCADE)
    invited = models.ManyToManyField(User, verbose_name="Participants" ,related_name="invited_user")
    date = models.DateTimeField("Creation date", auto_now_add=True)

    class Meta:
        verbose_name = "Chat's room"
        verbose_name_plural = "Chats' rooms"

class Chat(models.Model):
    """Model of chat"""

    room = models.ForeignKey(Room, verbose_name="Chat's room", on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name="User", on_delete=models.CASCADE)
    text = models.TextField("Message", max_length=500)
    date = models.DateTimeField("Creation date", auto_now_add=True)

    class Meta:
        verbose_name = "Message of room"
        verbose_name_plural = "Messages of room"